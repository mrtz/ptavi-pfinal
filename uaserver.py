"""holaa."""
# !/usr/bin/python3
# -- coding: utf-8 --
import sys
import socketserver
import os
import random
import secrets
import simplertp
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from uaclient import login
import xml.etree.ElementTree as ET


class scualquiera(ContentHandler):
    """holaa."""

    def __init__(self):
        """holaa."""
        self.account = {}
        self.uaserver = {}
        self.rtpaudio = {}
        self.regproxy = {}
        self.log = {}
        self.audio = {}
        # 6 diccionarios, uno por cada etiqueta,
        # dentro irán los atributos con sus valores
        self.content = []
        # lista vacía donde voy a almacenar las
        # etiquetas con sus atributos y el contenido de estos

    def startElement(self, name, attrs):
        """holaa."""
        if name == 'account':
            # De esta manera tomamos los atributos de las etiquetas
            self.account['username'] = attrs.get('username', "")
            self.account['passwd'] = attrs.get('passwd', "")
            # si no tiene valor queda vacío
            self.content.append([name, self.account])
            # esta lista guarda la etiqueta con su/sus atributos

        elif name == 'uaserver':
            self.uaserver['ip'] = attrs.get('ip', "")
            self.uaserver['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.uaserver])

        elif name == 'rtpaudio':
            self.rtpaudio['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.rtpaudio])

        elif name == 'regproxy':
            self.regproxy['ip'] = attrs.get('ip', "")
            self.regproxy['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.regproxy])

    def get_tags(self):
        """holaa."""
        return self.content


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccrtp = {}

    def handle(self):
        """Echo server class."""
        tupla = self.client_address

        while 1:
            # lee línea a línea lo que envía el cliente
            line = self.rfile.read()
            # si no hay más lineas salimos del bucle infinito
            if not line:
                break
            print(line, 'linee')
            metodo = line.decode('utf-8').split(' ')[0]
            metod = line.decode('utf-8').split(' ')[1]
            print('Recibo del proxy-- ', line.decode('utf-8'))
            print(metodo, 'meeeetoo')
            print(metod, 'meeeetoo')

            if metodo == "INVITE":
                print('entro en invite')
                print('Recibo del proxy-- ', line.decode('utf-8'))
                login(LOGFICH, 'Received from ', proxy_ip,
                      str(proxy_p), line.decode('utf-8'))

                line = "\r\n\r\n" + "v=0\r\n"
                line += "o=" + USER + ' ' + UASERVER_IP + "\r\n"
                line += "s=misesion\r\n" + 't=0\r\n'
                line += "m=audio " + rtpaudio_p + " RTP"

                res = 'SIP/2.0 100 Trying\r\n\r\n'
                res += 'SIP/2.0 180 Ringing\r\n\r\n'
                res += 'SIP/2.0 200 OK\r\n'
                res += 'Content-Type: application/sdp\r\n'
                res += 'Content-Length: ' + str(len(line))

                cabecera = res + line

                print('Enviando-- ', (cabecera))
                self.wfile.write(str.encode(cabecera))

                login(LOGFICH, 'Sent to ', proxy_ip,
                      str(proxy_p), cabecera)

            elif metodo == "BYE":
                login(LOGFICH, 'Received from ', proxy_ip, str(proxy_p),
                      line.decode('utf-8'))

                sip = 'SIP/2.0 200 OK\r\n\r\n'
                print('Enviando-- ', sip)
                self.wfile.write(bytes(sip, 'utf-8'))
                login(LOGFICH, 'Sent to ', proxy_ip,
                      str(proxy_p), sip)

            elif metodo == "ACK":
                print('recibo ackkkkkkkk')
                login(LOGFICH, 'Received from ', proxy_ip,
                      str(proxy_p), line.decode('utf-8'))

                repro = 'cvlc rtp://@127.0.0.1:'
                repro += rtpaudio_p + ' 2> /dev/null &'
                os.system(repro)
                print('Reproducimos' + repro)

                numero = random.randint(0, 1000)
                nbit = secrets.randbits(1)

                RTP_Header = simplertp.RtpHeader()
                print('fgbjek')
                RTP_Header.set_header(version=2, marker=nbit, payload_type=14,
                                      ssrc=numero)
                audio = simplertp.RtpPayloadMp3(AUDIOPATH)
                simplertp.send_rtp_packet(RTP_Header, audio,
                                          UASERVER_IP, int(rtpaudio_p))
                login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p),
                      line.decode('utf-8'))
                print('Envío audio ')
                # log

            elif metodo != 'ACK' and metodo != 'REGISTER' \
                    and metodo != 'INVITE':
                print('entro en metodo erroneo')
                nopermitido = 'SIP/2.0 405 Method Not Allowed\r\n\r\n'
                self.wfile.write(bytes(nopermitido, 'utf-8'))
                login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), nopermitido)

            else:
                badreq = 'SIP/2.0 400 Bad Request\n\r\n'
                self.wfile.write(bytes(badreq, 'utf-8'))
                login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), badreq)


if __name__ == "__main__":

    try:
        CONFI_ = str(sys.argv[1])

    except IndexError:
        sys.exit("Usage: python3 server.py config ")

    parser = make_parser()
    cHandler = scualquiera()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFI_))

    # Variables del XML ua1
    confi_ua1 = cHandler.get_tags()
    print(confi_ua1)
    USER = confi_ua1[0][1]['username']
    psswd = confi_ua1[0][1]['passwd']
    UASERVER_IP = confi_ua1[1][1]['ip']
    UASERVER_P = confi_ua1[1][1]['puerto']
    rtpaudio_p = confi_ua1[2][1]['puerto']
    proxy_ip = confi_ua1[3][1]['ip']
    proxy_p = confi_ua1[3][1]['puerto']

    # log y audio los importamos leyendo del archivo ua1.xml
    tree = ET.parse(CONFI_)
    root = tree.getroot()
    for w in root.findall('log'):  # Element.findall
        # encuentra elementos con una etiqueta que son
        # hijos del elemento actual
        log = w.text
    for w in root.findall('audio'):  # Element.findall
        # encuentra elementos con una etiqueta que so
        # n hijos del elemento actual
        audioe = w.text
    LOGFICH = log
    AUDIOPATH = audioe

    listen = 'listening...'
    print('listening...')
    print("Lanzando server UDP de eco...")
    login(LOGFICH, 'Starting ', proxy_ip, str(proxy_p), listen)

    print(UASERVER_P)

    serv = socketserver.UDPServer((UASERVER_IP, int(UASERVER_P)),
                                  EchoHandler)

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print('Fin')

"""holaa."""
# !/usr/bin/python3
# -- coding: utf-8 --
import socket
import sys
import random
import os
import secrets
import time
import simplertp
import hashlib
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import xml.etree.ElementTree as ET


class cualquiera(ContentHandler):
    """para obtener datos de archivo XML."""

    def __init__(self):
        """holaa."""
        self.account = {}
        self.uaserver = {}
        self.rtpaudio = {}
        self.regproxy = {}
        # 6 diccionarios, uno por cada etiqueta,
        # dentro irán los atributos con sus valores
        self.content = []
        # lista vacía donde voy a almacenar las etiquetas
        # con sus atributos y el contenido de estos

    def startElement(self, name, attrs):
        """holaa."""
        if name == 'account':
            # tomamos los atributos de las etiquetas
            self.account['username'] = attrs.get('username', "")
            self.account['passwd'] = attrs.get('passwd', "")
            # si no tiene valor queda vacío
            self.content.append([name, self.account])
            # esta lista guarda la etiqueta con sus atributos

        elif name == 'uaserver':
            self.uaserver['ip'] = attrs.get('ip', "")
            self.uaserver['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.uaserver])

        elif name == 'rtpaudio':
            self.rtpaudio['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.rtpaudio])

        elif name == 'regproxy':
            self.regproxy['ip'] = attrs.get('ip', "")
            self.regproxy['puerto'] = attrs.get('puerto', "")
            self.content.append([name, self.regproxy])

    def get_tags(self):
        """holaa."""
        return self.content


def login(log, tipo, ip, port, mensaje):  # tipo será sent o received
    """holaa."""
    maketime = time.mktime((time.gmtime()[0], time.gmtime()[1],
                           time.gmtime()[2], 0, 0, 0, 0, 0, 0))
    resta = time.time() - maketime
    resta = int(resta)
    f = open(log, "a")
    tiempo_actual = time.strftime('%Y-%m-%d',
                                  time.gmtime(time.time()))  # fecha
    mensaje = mensaje.replace('\r\n', " ")
    # los saltos de línea se transforman en espacios en blanco
    linea = str(tiempo_actual) + ' ' + str(resta)\
        + ' ' + tipo + ' ' + ip + ':' + str(port) + ' ' + mensaje
    f.write(linea + "\n")
    f.close()


if __name__ == "__main__":

    error = 'Usage: python uaclient.py config method option'
    try:

        CONF_ = str(sys.argv[1])
        METOD_ = str(sys.argv[2])
        OPC_ = str(sys.argv[3])

    except (IndexError, ValueError):
        sys.exit(error)
    parser = make_parser()
    cHandler = cualquiera()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONF_))

    confi_ua1 = cHandler.get_tags()

    # Variables del XML ua1
    confi_ua1 = cHandler.get_tags()
    USER = confi_ua1[0][1]['username']
    PSSWD = confi_ua1[0][1]['passwd']
    UASERVER_IP = confi_ua1[1][1]['ip']
    UASERVER_P = confi_ua1[1][1]['puerto']
    rtpaudio_p = confi_ua1[2][1]['puerto']
    proxy_ip = confi_ua1[3][1]['ip']
    proxy_p = confi_ua1[3][1]['puerto']
    # log y audio los importamos leyendo del archivo ua1.xml
    tree = ET.parse(CONF_)
    root = tree.getroot()
    for w in root.findall('log'):  # Element.findall
        # encuentra elementos con una etiqueta que son
        # hijos del elemento actual
        log = w.text
    for w in root.findall('audio'):  # Element.findall
        # encuentra elementos con una etiqueta que son
        # hijos del elemento actual
        audioe = w.text
    LOGFICH = log
    AUDIOPATH = audioe

    if METOD_ == 'REGISTER':
        LSIP = ('REGISTER sip:' + USER + ':' + UASERVER_P +
                ' SIP/2.0\r\n' + 'Expires: ' + OPC_ + '\r\n\r\n')
        print('Enviando al proxy: ' + LSIP + '\r\n')
        login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), LSIP)
    elif METOD_ == 'INVITE':

        SIP = METOD_ + ' sip:' + OPC_ + ' SIP/2.0\r\n'
        SIP += 'Content-Type: application/sdp\r\n'
        SIP += 'Content-Length: '

        LINE = "v=0\r\n"
        LINE += "o=" + USER + ' ' + UASERVER_IP + "\r\n"
        LINE += "s=misesion\r\n" + 't=0\r\n'
        LINE += "m=audio " + rtpaudio_p + " RTP"
        CL = str(len(bytes(LINE, 'utf-8'))) + "\r\n\r\n"

        LSIP = SIP + CL + LINE
        print('Enviando al proxy: ' + LSIP + '\r\n')
        login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), LSIP)
    elif METOD_ == 'BYE':
        LSIP = METOD_ + ' sip:' + OPC_ + ' SIP/2.0\r\n'
        print('Enviando al proxy: ' + LSIP + '\r\n')
        login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), LSIP)
    if METOD_ != 'BYE' and METOD_ != 'INVITE' and METOD_ != 'REGISTER':
        Error = 'Error: Introduce un método válido: REGISTER, INVITE, BYE'
        print(error)
        login(LOGFICH, ' Error ', '', '', Error)
    # me voy a conectar con el proxy

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxy_ip, int(proxy_p)))
        my_socket.send(bytes(LSIP, 'utf-8'))
        # recibiendo del proxy
        data = my_socket.recv(1024)
        line = data.decode('utf-8')
        linep = line.split('\r\n')
        print('Recibido -- ', line)
        login(LOGFICH, 'Received from ', proxy_ip, str(proxy_p), line)
        decode = line.split()
        # if '401' in line:

        if line.startswith('SIP/2.0 401 Unauthorized\r\n'):

            nonce = line.split('"')[1]
            print('hola', nonce)
            key = hashlib.blake2b()
            key.update(bytes(nonce, 'utf-8'))
            key.update(bytes(PSSWD, 'utf-8'))
            nnonce = key.hexdigest()
            lin = METOD_ + ' sip:' + USER + ':' + UASERVER_P \
                + ' SIP/2.0\r\n' + 'Expires: ' + OPC_ + '\r\n\r\n'
            respuesta = lin + "Authorization: Digest response= " + '"' \
                + nnonce + '"\r\n'
            print('Enviando... ' + respuesta)
            print(nnonce, 'dfhuieo')
            login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), respuesta)
            my_socket.send(bytes(respuesta, 'utf-8') + b'\r\n')
            print("Terminando socket...")

        if METOD_ == 'INVITE':

            manda = "SIP/2.0 100 Trying\r\n" \
                            "SIP/2.0 180 Ringing\r\n " \
                            "SIP/2.0 200 OK\r\n"
            print(line, 'line')
            if manda in line:
                ack = 'ACK sip:' + OPC_ + ' SIP/2.0\r\n'
                print('Enviando... ' + ack)
                login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), ack)
                repro = 'cvlc rtp://@127.0.0.1:'  # reptoductor
                repro += rtpaudio_p + ' 2> /dev/null &'
                os.system(repro)
                print('Reproducimos' + repro)
                my_socket.send(bytes(ack, 'utf-8'))

                # envío del audio al proxy
                numero = random.randint(0, 99999)
                nbit = secrets.randbits(1)
                RTP_Header = simplertp.RtpHeader()
                print('fgbjek')
                RTP_Header.set_header(version=2, marker=nbit,
                                      payload_type=14, ssrc=numero)
                audio = simplertp.RtpPayloadMp3(AUDIOPATH)
                simplertp.send_rtp_packet(RTP_Header, audio,
                                          UASERVER_IP, int(rtpaudio_p))
                my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                my_socket.connect(('127.0.0.1', int('5456')))
                my_socket.send(bytes(ack, 'utf-8'))
                # login(LOGFICH, 'Sent to ', proxy_ip, str(proxy_p), ack)

            print("Terminando socket...")
